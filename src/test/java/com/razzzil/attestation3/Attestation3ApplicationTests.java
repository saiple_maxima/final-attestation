package com.razzzil.attestation3;

import com.razzzil.attestation3.model.Product;
import com.razzzil.attestation3.service.ProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class Attestation3ApplicationTests {

    @Autowired
    private ProductService productService;

    @Test
    void testSaleProducts() {
        List<Product> productList = productService.getSaleProducts();
        for (Product product : productList) {
            Assertions.assertTrue(product.getPrice() <= 1000);
        }
    }

}
