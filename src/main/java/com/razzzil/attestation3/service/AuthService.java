package com.razzzil.attestation3.service;

import com.razzzil.attestation3.model.Product;
import com.razzzil.attestation3.model.WebUser;
import com.razzzil.attestation3.repository.WebUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final WebUserRepository webUserRepository;
    private final PasswordEncoder passwordEncoder;

    public WebUser register(String name, String login, String password){
        String passwordHash = passwordEncoder.encode(password);
        WebUser webUser = WebUser.builder()
                .passwordHash(passwordHash)
                .login(login)
                .name(name)
                .build();
        return webUserRepository.save(webUser);
    }

    public WebUser getById(long id) {
        return webUserRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("User with " + id + " is not presented in DB"));
    }

    public WebUser save(WebUser user) {
        return webUserRepository.save(user);
    }



}


