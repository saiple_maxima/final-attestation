package com.razzzil.attestation3.service;

import com.razzzil.attestation3.model.Product;
import com.razzzil.attestation3.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product save(String name, String picture, int price) {
        Product product = Product.builder()
                .picture(picture)
                .price(price)
                .name(name)
                .build();
        return productRepository.save(product);
    }

    public Product update(String name, String picture, int price, long id) {
        Product product = Product.builder()
                .picture(picture)
                .price(price)
                .name(name)
                .id(id)
                .build();
        return productRepository.save(product);
    }

    public void delete(long id) {
        productRepository.deleteById(id);
    }

    public Product getById(long id) {
        return productRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Product with " + id + " is not presented in DB"));
    }

    public void deleteFromCart(long userId, long productId) {
        productRepository.deleteProductFromCart(userId, productId);
    }

    public List<Product> getSaleProducts(){
        return productRepository.getSaleProducts();
    }

}
