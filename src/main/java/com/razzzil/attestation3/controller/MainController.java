package com.razzzil.attestation3.controller;

import com.razzzil.attestation3.model.Product;
import com.razzzil.attestation3.model.WebUser;
import com.razzzil.attestation3.security.UserDetailsImpl;
import com.razzzil.attestation3.service.AuthService;
import com.razzzil.attestation3.service.ProductService;
import jakarta.annotation.security.PermitAll;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/")
public class MainController {

    private final ProductService productService;

    private final AuthService authService;

    @GetMapping
    @PermitAll
    public String main(Model model, Authentication authentication) {
        if (authentication != null) {
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            WebUser webUser = userDetails.getWebUser();
            model.addAttribute("user", webUser);
        }
        List<Product> products = productService.getAllProducts();
        List<Product> saleProducts = productService.getSaleProducts();
        model.addAttribute("products", products);
        model.addAttribute("sale", saleProducts);
        return "main";
    }

    @GetMapping("/user")
    @PreAuthorize("isAuthenticated()")
    @Transactional
    public String user(Authentication authentication, Model model) {
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        WebUser webUser = userDetails.getWebUser();
        WebUser fromDb = authService.getById(webUser.getId());
        model.addAttribute("user", webUser);
        model.addAttribute("products", fromDb.getCart());
        return "user";
    }

    @PostMapping("/addToCart/{id}")
    @PreAuthorize("isAuthenticated()")
    @Transactional
    public String addToCart(@PathVariable("id") long productId, Authentication authentication) {
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        WebUser webUser = userDetails.getWebUser();
        WebUser fromDb = authService.getById(webUser.getId());
        fromDb.getCart().add(Product.builder().id(productId).build());
        authService.save(webUser);
        return "redirect:/user";
    }

    @PostMapping("/removeFromCart/{id}")
    @PreAuthorize("isAuthenticated()")
    @Transactional
    public String removeFromCart(@PathVariable("id") long productId, Authentication authentication) {
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        WebUser webUser = userDetails.getWebUser();
        productService.deleteFromCart(webUser.getId(), productId);
        return "redirect:/user";
    }

}
