package com.razzzil.attestation3.controller;

import com.razzzil.attestation3.model.Product;
import com.razzzil.attestation3.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/admin")
@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
public class AdminController {

    private final ProductService productService;

    @GetMapping
    public String admin(Model model){
        List<Product> products = productService.getAllProducts();
        model.addAttribute("products", products);
        return "admin";
    }

    @GetMapping("/adminUpdateProduct/{id}")
    public String adminUpdateProduct(@PathVariable("id") long id, Model model){
        Product product = productService.getById(id);
        model.addAttribute("product", product);
        return "adminUpdateProduct";
    }

    @PostMapping("/save")
    public String save(@RequestParam("name") String name,
                        @RequestParam("picture") String picture,
                        @RequestParam("price") int price) {
        productService.save(name, picture, price);
        return "redirect:/admin";
    }

    @PostMapping("/update/{id}")
    public String update(@RequestParam("name") String name,
                        @RequestParam("picture") String picture,
                        @RequestParam("price") int price,
                        @PathVariable("id") long id) {
        productService.update(name, picture, price, id);
        return "redirect:/admin";
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id) {
        productService.delete(id);
        return "admin";
    }
}
