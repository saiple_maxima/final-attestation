package com.razzzil.attestation3.repository;

import com.razzzil.attestation3.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    @Modifying
    @Query(nativeQuery = true, value = "delete from cart where web_user_id = :userId and product_id = :productId")
    void deleteProductFromCart(long userId, long productId);

    @Query(nativeQuery = true, value = "select * from product where price <= 1000")
    List<Product> getSaleProducts();
}
