package com.razzzil.attestation3.repository;

import com.razzzil.attestation3.model.WebUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WebUserRepository extends JpaRepository<WebUser, Long> {

    Optional<WebUser> findByLogin(String login);
}
