package com.razzzil.attestation3.model;

public enum UserRole {
    ROLE_USER,
    ROLE_ADMIN
}
