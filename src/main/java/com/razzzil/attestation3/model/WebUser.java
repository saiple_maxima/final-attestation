package com.razzzil.attestation3.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
public class WebUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, unique = true)
    private String login;

    @Column(nullable = false)
    private String passwordHash;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    @ColumnDefault("'USER'")
    @Builder.Default
    @Enumerated(EnumType.STRING)
    private UserRole userRole = UserRole.ROLE_USER;

    @ManyToMany
    @JoinTable(
            name = "cart",
            joinColumns = { @JoinColumn(name = "web_user_id") },
            inverseJoinColumns = { @JoinColumn(name = "product_id") }
    )
    @Builder.Default
    private List<Product> cart = new ArrayList<>();
}
